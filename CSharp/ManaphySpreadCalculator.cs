void Main()
{
    // Initial state
    // s0 should be s1, s0 from CS
    // s1 should be s3, s2 from CS
    ulong s0 = 0x0123456789ABCDEF;
    ulong s1 = 0x0123456789ABCDEF;

    // Set this for how far you want to search.
    int maxAdvance = 500000;

    var rng = new XorShift(s0, s1);
    for (int advances = 0; advances < maxAdvance; advances++)
    {
        GetStateManaphy(advances, rng);
        rng.Next();
    }
}

// You can define other methods, fields, classes and namespaces here
private void GetStateManaphy(int advance, XorShift rng)
{
    int FlawlessIVs = 3;
    var output = $"{advance} | {rng.s0:x16} | {rng.s1:x16} |";

    var ec = rng.Next();
    output = output + $" {ec:x8} |";
    var pid = rng.Next();
    output = output + $" {pid:x8} |";
    rng.Next(); //idk what this is but we can ignore it.

    int[] ivs = {-1, -1, -1, -1, -1, -1};
    var i = 0;
    while (i < FlawlessIVs)
    {
        var next = rng.Next();
        next = next - (next / 6) * 6;
        if (ivs[next] == -1)
        {
            ivs[next] = 31;
            i++;
        }
    }

    var ivstring = "";
    for (i = 0; i < 6; i++)
    {
        if (ivs[i] == -1)
        {
            var next = rng.NextInt(32);
            ivs[i] = (int)next;
        }
        ivstring += ivs[i];
        if (i < 5)
            ivstring += "/";
    }

    output = output + $" {ivstring} |";

    var nature = rng.Next();
    nature = nature - (nature / 25) * 25;
    output = output + $" {nature}";

    // Add your filters here.
    if (nature != 10)
        return;
    if (ivs[0] != 31 || ivs[1] != 0 || ivs[2] != 31 || ivs[3] != 31 || ivs[4] != 31 || ivs[5] != 31)
        return;
        
    Console.WriteLine(output);
}

public struct XorShift
{
    public ulong s0, s1;

    public XorShift(ulong is0, ulong is1)
    {
        s0 = is0;
        s1 = is1;
    }

    public (ulong, ulong) GetState()
    {
        return (s0, s1);
    }

    /// <summary>
    /// Gets the next random <see cref="ulong"/>.
    /// </summary>
    public ulong Next()
    {
        var _s0 = s0 & 0xFFFFFFFF;
        var _s1 = s1 >> 32;

        _s0 ^= (_s0 << 11) & 0xFFFFFFFF;
        _s0 ^= (_s0 >> 8);
        _s0 ^= (_s1 ^ (_s1 >> 19));

        // Final calculations and store back to fields        
        s0 = ((s1 & 0xFFFFFFFF) << 32) | (s0 >> 32);
        s1 = _s0 << 32 | (s1 >> 32);

        var result = ((_s0 % 0xFFFFFFFF) + 0x80000000) & 0xFFFFFFFF;
        return result;
    }

    /// <summary>
    /// Gets a random value that is less than <see cref="MOD"/>
    /// </summary>
    /// <param name="MOD">Maximum value (exclusive). Generates a bitmask for the loop.</param>
    /// <returns>Random value</returns>
    public ulong NextInt(ulong MOD = 0xFFFFFFFF)
    {
        ulong mask = GetBitmask(MOD);
        ulong res;
        do
        {
            res = Next() & mask;
        } while (res >= MOD);
        return res;
    }

    /// <summary>
    /// Next Power of Two
    /// </summary>
    private static ulong GetBitmask(ulong x)
    {
        x--; // comment out to always take the next biggest power of two, even if x is already a power of two
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        return x;
    }
}